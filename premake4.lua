#!lua

-- A solution contains projects, and defines the available configurations
solution "clox"
   configurations { "Debug", "Release" }

   -- A project defines one build target
   project "clox"
      kind "ConsoleApp"
      language "C"
      buildoptions {
         "-std=c11",
         "-Wall",
         "-O2"
      }

      files { "**.h", "**.c" }

      configuration "Debug"
         defines { "DEBUG" }
         flags { "Symbols" }

      configuration "Release"
         defines { "NDEBUG" }
         flags { "Optimize" }